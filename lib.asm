section .text
%define    WRITE   1
%define    STDOUT  1
%define    EXIT    60

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error


exit: 
    mov rax, EXIT 
    syscall


string_length:
    xor rax, rax
.loop:
   cmp  byte [rdi + rax], 0    ; Сравниваем с нуль-терминатором (предполагается, что указатель находится в rdi)
    je   .end ; Заканчиваем, если нуль-терминтор
    inc  rax ; Увеличиваем счетчик
    jmp  .loop ; Возврат к началу цикла
.end:
    ret


print_string:
    xor rax, rax
    push rdi
    call string_length
    mov rdx, rax ; Сохраням длину строки
    pop rsi
    mov rax, WRITE ; Оператор "write"
    mov rdi, STDOUT ; Вывод в stdout 
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp ; Указываем адрес символа
    mov rax, 1 ; 
    mov rdi, 1 
    mov rdx, 1
    syscall
    pop rdi  ; Возврат указателя стека
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; Ищем знак
    jge .end ; Выходим если плюс
    neg rdi
    push rdi ; Сохраняем модуль
    mov rdi, '-' ;
    call print_char ; Пишем минус
    pop rdi
.end
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10
    mov r9, 1
    dec rsp
    mov byte [rsp], 0

.loop:
    inc r9
    xor rdx, rdx
    div r8
    add dl, '0' ; Переводим в ASCII
    dec rsp
    mov byte [rsp], dl
    test rax, rax   ; Проверяем закончили ли деление
    jne .loop
.print:
    mov rdi, rsp
    push r9
    call print_string
    pop r9
    add rsp, r9 ; Возврат указателя
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
.loop:
    movzx r8, byte[rdi]
    movzx r9, byte[rsi]
    cmp r8, r9 ; Сравниваем побитово
    jne .neq ; Выходим, если не совпало
    test r8, r8; Сравниваем с нуль-терминатором
    je .eq ; Выходим, если конец
    inc rdi
    inc rsi
    jmp .loop
.neq: ; Not equals
    xor rax, rax
    ret
.eq: ; Equals
    mov rax, 1
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rsi, rsp ; Инициализируем буфер для чтения
    xor rax, rax  ; Оператор чтения
    xor rdi, rdi ; stdin
    mov rdx, 1 ; Длина строки
    syscall

    test rax, rax
    je .end
    mov al, [rsp] ; Учитываем размер операндов
    add rsp, 1
    ret 
.end:
    xor rdi, rdi
    add rsp, 1
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14

.loop:
    call read_char
    test rax, rax
    jz .done
    cmp r14, r13
    jnl .err
    cmp rax, 0x20
    jz .skip
    cmp rax, 0x9
    jz .skip
    cmp rax, 0xA
    jz .skip
    mov [r12 + r14], al
    inc r14
    jmp .loop

.skip:
    test r14, r14
    jz .loop

.done:
    mov byte[r12 + r14], 0
    mov rax, r12
    mov rdx, r14
    jmp .exit

.err:
    xor rax, rax

.exit:
    pop r14
    pop r13
    pop r12
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx; Инициализируем счетчик
.loop:
    movzx rcx, byte [rdi + rdx] ; Учитываем размер операндов
    test rcx, rcx
    jz .end
    cmp rcx, '0'; Замечаем, что код цифр находится в некотором диапазоне.
    jb .end
    cmp rcx, '9'
    ja .end
    inc rdx
    sub rcx, '0'
    imul rax, rax, 10
    add rax, rcx
    jmp .loop
.end:
    test rdx, rdx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-' ;Ищем знак
    je .neg
    call parse_uint
    ret 
  .neg: ; Если минус
    inc rdi
    call parse_uint
    neg rax ; Получаем модуль
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax ; Инициализируем длину
  .loop:
    cmp rdx, rax ; Сравнение длины буфера
    je .end
    mov r8b, byte [rdi+rax] ; Учитываем размер операндов
    mov [rsi +rax], r8b
    inc rax
    test r8b, r8b
    jne .loop
    ret
  .end:
    xor rax, rax
    ret


print_error:
    push rdi
    call string_length
    mov  rdx, rax
    pop  rsi
    mov  rax, 1
    mov  rdi, 2
    syscall
    call print_newline
    ret
