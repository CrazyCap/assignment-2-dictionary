%include "lib.inc"
%define keysize 8
global find_word

find_word:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi

    .loopz:
        mov rdi, [r13]
        test rdi, rdi
        jz .not_found
        add rdi, keysize
        mov rsi, r12
        call string_equals
        test rax, rax
        jnz .found
        mov r13, [r13]
        jmp .loopz

    .found:
        mov rax, [r13]
        pop r13
        pop r12
        ret

    .not_found:
        xor rax, rax
	pop r13
        pop r12
        ret