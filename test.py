import subprocess

inputs = ["R", "K", "P"]
outputs = ["Rahim", "Kenzhaev", "P3233"]
errors = ["", "", ""]

for i in range(len(inputs)):
    subproc = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = subproc.communicate(input=inputs[i].encode())
    out = stdout.decode().strip()

    if out == outputs[i]:
        print("Complete {}".format(i + 1))
    else:
        print("Failed {}".format(i + 1))

