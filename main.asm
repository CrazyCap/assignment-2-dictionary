%include "words.inc"
%include "lib.inc"
%include "dict.inc"

%define bsize 255
%define keysize 8

section .rodata
not_found db "NOT FOUND", 0
too_long db "TOO LONG", 0

section .bss
buf resb (bsize+1)

section .text

global _start


_start:
    mov rdi, buf
    mov rsi, bsize
    call read_word

    test rax, rax
    jz .er_too_long
    push rdx
    mov rdi, rax
    mov rsi, pos
    call find_word

    test rax, rax
    jz .er_not_found
    pop rdx
    mov rdi, rax
    add rdi, keysize
    add rdi, rdx
    inc rdi
    call print_string
    xor rdi, rdi
    jmp exit

.er_too_long:
    mov rdi, too_long
    jmp .exit
    
.er_not_found:
    mov rdi, not_found

.exit:
    call print_error
    mov rdi, 1
    jmp exit
