NASM = nasm
NASMFLAGS = -felf64
LD = ld

.PHONY: all clean test

all: main

lib.o: lib.asm
	$(NASM) $(NASMFLAGS) lib.asm -o lib.o

dict.o: dict.asm lib.inc
	$(NASM) $(NASMFLAGS) dict.asm -o dict.o

main.o: main.asm lib.inc dict.inc words.inc
	$(NASM) $(NASMFLAGS) main.asm -o main.o

main: main.o lib.o dict.o
	$(LD) -o main $^

clean:
	rm -f *.o main

test:
	python3 test.py